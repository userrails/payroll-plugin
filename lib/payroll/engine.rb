module Payroll
  class Engine < ::Rails::Engine
    isolate_namespace Payroll
  end
end
