require_dependency "payroll/application_controller"

module Payroll
  class LeaveTypesController < ApplicationController
    def index
      @leave_types = Payroll::LeaveType.all
    end

    def new
      @leave_type = Payroll::LeaveType.new
    end

    def create
      @leave_type = Payroll::LeaveType.new(leave_type_params)
      if @leave_type.save
        redirect_to leave_types_path
      else
        render :new
      end
    end

    private

    def leave_type_params
      params.require(:leave_type).permit(:title)
    end
  end
end
