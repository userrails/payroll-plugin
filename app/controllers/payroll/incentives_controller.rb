require_dependency "payroll/application_controller"

module Payroll
  class IncentivesController < ApplicationController
    before_action :find_employee

    def index
      @incentives = Payroll::Incentive.all
    end

    def new
      @incentive = Payroll::Incentive.new
    end

    def create
      @incentive = Payroll::Incentive.new(incentive_params.merge(employee_id: @employee.id))
      if @incentive.save
        redirect_to employee_incentives_path(@employee)
      else
        render :new
      end
    end

    private

    def incentive_params
      params.require(:incentive).permit(:hra, :da, :ta, :medical, :remarks)
    end

    def find_employee
      @employee = Payroll::Employee.find(params[:employee_id])
    end
  end
end
