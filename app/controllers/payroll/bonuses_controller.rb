require_dependency "payroll/application_controller"

module Payroll
  class BonusesController < ApplicationController
    before_action :find_employee

    def index
      @bonuses = Payroll::Bonus.all
    end


    def new
      @bonus = Payroll::Bonus.new
    end

    def create
      @bonus = Payroll::Bonus.new(bonus_params.merge(employee_id: @employee.id))
      if @bonus.save
        redirect_to employee_bonuses_path(@employee)
      else
        render :new
      end
    end

    private

    def bonus_params
      params.require(:bonus).permit(:employee_id, :name, :amount, :remarks)
    end

    def find_employee
      @employee = Payroll::Employee.find(params[:employee_id])
    end
  end
end
