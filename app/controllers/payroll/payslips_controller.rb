require_dependency "payroll/application_controller"

module Payroll
  class PayslipsController < ApplicationController
    def index
      @employee = Payroll::Employee.find(params[:employee_id])
      @incentive = @employee.incentives.last
      @deduction = @employee.deductions.last
    end
  end
end
