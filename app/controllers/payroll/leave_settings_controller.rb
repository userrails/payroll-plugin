require_dependency "payroll/application_controller"

module Payroll
  class LeaveSettingsController < ApplicationController
    before_action :find_employee

    def index
      @leave_settings = Payroll::LeaveSetting.all
    end

    def new
      @leave_setting = Payroll::LeaveSetting.new
    end

    def create
      @leave_setting = Payroll::LeaveSetting.new(leave_setting_params.merge(employee_id: @employee.id))
      if @leave_setting.save
        redirect_to employee_leave_settings_path(@employee.id)
      else
        render :new
      end
    end

    private

    def leave_setting_params
      params.require(:leave_setting).permit(:leave_type_id, :jan, :feb, :mar, :apr, :may, :jun, :jul, :aug, :sep, :oct, :nov, :dec)
    end

    def find_employee
      @employee = Payroll::Employee.find(params[:employee_id])
    end
  end
end
