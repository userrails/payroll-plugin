require_dependency "payroll/application_controller"

module Payroll
  class SalaryInfosController < ApplicationController
    before_action :find_employee

    def index
      @salary_infos = Payroll::SalaryInfo.all
    end

    def new
      @salary_info = Payroll::SalaryInfo.new
    end

    def create
      @salary_info = Payroll::SalaryInfo.new(salary_info_params.merge(employee_id: @employee.id))
      if @salary_info.save
        redirect_to employee_salary_infos_path(@employee)
      else
        render :new
      end
    end

    private

    def salary_info_params
      params.require(:salary_info).permit(:salary_type, :salary_amount, :remarks)
    end

    def find_employee
      @employee = Payroll::Employee.find(params[:employee_id])
    end
  end
end
