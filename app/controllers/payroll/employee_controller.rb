require_dependency "payroll/application_controller"

module Payroll
  class EmployeeController < ApplicationController
    def index
      @employee = Payroll::Employee.all
    end

    def new
      @employee = Payroll::Employee.new
    end

    def create
      @employee = Payroll::Employee.new(employee_params)
      if @employee.save
        redirect_to employee_index_path
      else
        render :new
      end
    end

    private

    def employee_params
      params.require(:employee).permit(:name, :father, :gender, :date_of_birth, :address, :city, :state, :pincode, :contact_no, :designation, :department, :date_of_joining, :photo, :remarks)
    end
  end
end
