require_dependency "payroll/application_controller"

module Payroll
  class DeductionsController < ApplicationController
    before_action :find_employee

    def index
      @deductions = Payroll::Deduction.all
    end

    def new
      @deduction = Payroll::Deduction.new
    end

    def create
      @deduction = Payroll::Deduction.new(deduction_params.merge(employee_id: @employee.id))
      if @deduction.save
        redirect_to employee_deductions_path(@employee)
      else
        render :new
      end
    end

    private

    def deduction_params
      params.require(:deduction).permit(:advance_pay, :professional_tax, :loan, :provisional_fund, :remarks)
    end

    def find_employee
      @employee = Payroll::Employee.find(params[:employee_id])
    end
  end
end
