require_dependency "payroll/application_controller"

module Payroll
  class LeaveEntriesController < ApplicationController
    before_action :find_employee

    def index
      @leave_entries = Payroll::LeaveEntry.all
    end

    def new
      @leave_entry = Payroll::LeaveEntry.new
    end

    def create
      @leave_entry = Payroll::LeaveEntry.new(leave_entry_params.merge(employee_id: @employee.id))
      if @leave_entry.save
        redirect_to employee_leave_entries_path(@employee)
      else
        render :new
      end
    end

    private

    def leave_entry_params
      params.require(:leave_entry).permit(:leave_type_id, :total_leave, :leave_from, :leave_to, :balance_leave)
    end

    def find_employee
      @employee = Payroll::Employee.find(params[:employee_id])
    end
  end
end
