module Payroll
  class LeaveEntry < ApplicationRecord
    belongs_to :leave_type

    scope :absent, -> (emp_id) {
      obj = Payroll::LeaveType.where(title: 'absent').first
      where("employee_id=? and leave_type_id=?", emp_id, obj.id)
    }
    scope :sickness, -> (emp_id) {
      obj = Payroll::LeaveType.where(title: 'sickness').first
      where("employee_id=? and leave_type_id=?", emp_id, obj.id)
    }
    scope :break, -> (emp_id) {
      obj = Payroll::LeaveType.where(title: 'break').first
      where("employee_id=? and leave_type_id=?", emp_id, obj.id)
    }
    scope :casual, -> (emp_id) {
      obj = Payroll::LeaveType.where(title: 'casual').first
      where("employee_id=? and leave_type_id=?", emp_id, obj.id)
    }
  end
end
