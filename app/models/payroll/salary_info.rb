module Payroll
  class SalaryInfo < ApplicationRecord
    validates_presence_of :employee_id, :salary_type, :salary_amount, :remarks
    belongs_to :employee
  end
end
