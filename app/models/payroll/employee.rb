module Payroll
  class Employee < ApplicationRecord
    has_many :leave_entries
    has_many :salary_infos
    has_many :incentives
    has_many :deductions
    has_many :bonuses

    validates_presence_of :name

    enum gender: [:female, :male]
  end
end
