class CreatePayrollEmployees < ActiveRecord::Migration[5.1]
  def change
    create_table :payroll_employees do |t|
      t.string :name
      t.string :father
      t.integer :gender
      t.date :date_of_birth
      t.string :address
      t.string :city
      t.string :state
      t.string :pincode
      t.string :contact_no
      t.string :designation
      t.string :department
      t.date :date_of_joining
      t.string :photo
      t.text :remarks

      t.timestamps
    end
  end
end
