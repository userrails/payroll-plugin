class CreatePayrollLeaveSettings < ActiveRecord::Migration[5.1]
  def change
    create_table :payroll_leave_settings do |t|
      t.integer :employee_id
      t.integer :leave_type_id
      t.integer :jan
      t.integer :feb
      t.integer :mar
      t.integer :apr
      t.integer :may
      t.integer :jun
      t.integer :jul
      t.integer :aug
      t.integer :sep
      t.integer :oct
      t.integer :nov
      t.integer :dec

      t.timestamps
    end
  end
end
