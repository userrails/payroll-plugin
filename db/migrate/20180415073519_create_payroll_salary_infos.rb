class CreatePayrollSalaryInfos < ActiveRecord::Migration[5.1]
  def change
    create_table :payroll_salary_infos do |t|
      t.integer :employee_id
      t.string :salary_type
      t.decimal :salary_amount, precision: 10, scale: 2
      t.text :remarks

      t.timestamps
    end
  end
end
