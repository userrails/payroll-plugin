class CreatePayrollBonus < ActiveRecord::Migration[5.1]
  def change
    create_table :payroll_bonus do |t|
      t.integer :employee_id
      t.string :name
      t.decimal :amount
      t.text :remarks

      t.timestamps
    end
  end
end
