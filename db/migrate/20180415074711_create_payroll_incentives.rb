class CreatePayrollIncentives < ActiveRecord::Migration[5.1]
  def change
    create_table :payroll_incentives do |t|
      t.integer :employee_id
      t.float :hra
      t.float :da
      t.float :ta
      t.float :medical
      t.text :remarks

      t.timestamps
    end
  end
end
