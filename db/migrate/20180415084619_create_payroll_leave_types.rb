class CreatePayrollLeaveTypes < ActiveRecord::Migration[5.1]
  def change
    create_table :payroll_leave_types do |t|
      t.string :title

      t.timestamps
    end
  end
end
