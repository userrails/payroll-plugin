class CreatePayrollDeductions < ActiveRecord::Migration[5.1]
  def change
    create_table :payroll_deductions do |t|
      t.integer :employee_id
      t.decimal :advance_pay
      t.decimal :professional_tax
      t.decimal :loan
      t.decimal :provisional_fund
      t.text :remarks

      t.timestamps
    end
  end
end
