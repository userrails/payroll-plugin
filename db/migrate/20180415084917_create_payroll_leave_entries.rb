class CreatePayrollLeaveEntries < ActiveRecord::Migration[5.1]
  def change
    create_table :payroll_leave_entries do |t|
      t.integer :employee_id
      t.integer :leave_type_id
      t.integer :total_leave
      t.date :leave_from
      t.date :leave_to
      t.integer :balance_leave

      t.timestamps
    end
  end
end
