Payroll::Engine.routes.draw do
  resources :employee, only: [:new, :create, :index] do
    resources :salary_infos, only: [:new, :create, :index]
    resources :incentives, only: [:new, :create, :index]
    resources :bonuses, only: [:new, :create, :index]
    resources :deductions, only: [:new, :create, :index]
    resources :leave_entries, only: [:new, :create, :index]
    resources :leave_settings, only: [:new, :create, :index]
    resources :payslips, only: :index
  end
  resources :leave_types, only: [:new, :create, :index]
end
