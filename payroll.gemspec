$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "payroll/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "payroll"
  s.version     = Payroll::VERSION
  s.authors     = ["Shiv Raj Badu"]
  s.email       = ["shivrajbadu@gmail.com"]
  s.homepage    = "http://127.0.0.1:3000"
  s.summary     = "Payroll feature"
  s.description = "Employer payroll feature has been implemented"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", "~> 5.1.4"
  s.add_dependency 'bootstrap', '~> 4.0.0.beta2'
  s.add_dependency 'simple_form'
  s.add_dependency 'jquery-rails'
end
